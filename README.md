#OSX Terminal Commands to simplify certain issues(Maverics)

### Delay the appearance of dock by 100s

	defaults write com.apple.dock autohide-time-modifier -int 100; 
	killall Dock

### Restore the default behavior:

	defaults delete com.apple.dock autohide-time-modifier; 
	killall Dock

###Font  smoothing

	defaults -currentHost write -globalDomain AppleFontSmoothing -int 3

###Mavericks sleep and display times: Create a difference

	sudo pmset -b sleep 10  # sleep after 10 minutes on battery; use System Preferences to set display off times

###Show library folder in the finder

	chflags nohidden /Users/[username]/Library/

###Remove DashBoard

	defaults write com.apple.dashboard mcx-disabled -boolean YES;
	killall Dock



###Uninstall Homebrew and all its associated packages:

    cd `brew --prefix`
    rm -rf Cellar
    brew prune
    rm `git ls-files`
    rm -r Library/Homebrew Library/Aliases Library/Formula Library/Contributions
    rm -rf .git
    rm -rf ~/Library/Caches/Homebrew

This should also revert your     /usr/local folder to its pre-Homebrew days. See the Homebrew installation wiki for more information.

Note: You may also need to remove ~/.homebrew as well. If you happen to have ~/.rvm then you should delete     ~/.rvm/bin/brew.
